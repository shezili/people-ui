import { mount, shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PersonCard from '../../app/components/PersonCard';
import React from 'react';

configure({ adapter: new Adapter() });

test('PersonCard calls handlePersonEdit when add button is clicked', () => {
  const person = {
    id: '1',
    name: { title: 'ms', first: 'test', last: 'user' },
  };
  const handlePersonEdit = jest.fn();
  const handlePersonDelete = jest.fn();

  const wrapper = mount(
    <PersonCard
      personData={person}
      cardClass="person-card"
      actionButtonText="Edit"
      handleEditOnClick={handlePersonEdit}
      handleDeleteOnClick={handlePersonDelete}
    />
  );

  const addBtn = wrapper.find('.ant-btn .edit-btn');
  addBtn.simulate('click');
  expect(handlePersonEdit).toBeCalledWith(person);
});

test('PersonCard calls handlePersonDelete when delete button is clicked', () => {
  const person = {
    id: '1',
    name: { title: 'ms', first: 'test', last: 'user' },
  };
  const handlePersonEdit = jest.fn();
  const handlePersonDelete = jest.fn();

  const wrapper = mount(
    <PersonCard
      personData={person}
      cardClass="person-card"
      actionButtonText="Edit"
      handleEditOnClick={handlePersonEdit}
      handleDeleteOnClick={handlePersonDelete}
    />
  );

  const deleteBtn = wrapper.find('.ant-btn .delete-btn');
  deleteBtn.simulate('click');
  expect(handlePersonDelete).toBeCalledWith(person);
});
