import {
  Dropdown,
  Select,
  Button,
  Modal,
  Form,
  Input,
  Radio,
  DatePicker,
  Icon,
} from 'antd';
const FormItem = Form.Item;
import React, { Component } from 'react';
import moment from 'moment';

const dateFormat = 'YYYY-MM-DD';
const Option = Select.Option;
export default class EditForm extends React.Component {
  render() {
    const { visible, onCancel, onCreate, form, personData } = this.props;
    const { getFieldDecorator } = this.props.form;
    let title = '',
      first = '',
      last = '',
      gender = '',
      email = '',
      dob = '',
      mobile = '',
      avatar = '',
      id = '',
      titleText = '',
      okText = '';

    if (personData != null) {
      titleText = `Edit details for ${personData.name.first} ${
        personData.name.last
      }`;
      title = personData.name.title;
      first = `${personData.name.first}`;
      last = personData.name.last;
      gender = personData.gender;
      email = personData.email;
      dob = personData.dob;
      mobile = personData.cell;
      avatar = personData.picture;
      id = personData.id;
      okText = 'Update';
    } else {
      titleText = 'Create new person';
      title = 'mr';
      dob = '1999-01-01';
      gender = 'male';
      okText = 'Create';
    }

    return (
      <Modal
        visible={visible}
        title={titleText}
        okText={okText}
        onCancel={onCancel}
        onOk={onCreate}
      >
        <Form layout="vertical">
          <FormItem label="Title">
            {getFieldDecorator('title', {
              rules: [
                {
                  required: true,
                },
              ],
              initialValue: title,
            })(
              <Select style={{ width: '22%' }}>
                <Option value="mr">Mr</Option>
                <Option value="mrs">Mrs</Option>
                <Option value="ms">Ms</Option>
                <Option value="miss">Miss</Option>
              </Select>
            )}
          </FormItem>
          <FormItem label="First Name">
            {getFieldDecorator('first', {
              rules: [
                {
                  required: true,
                  message: 'Please input the title of collection!',
                },
              ],
              initialValue: first,
            })(<Input />)}
          </FormItem>
          <FormItem label="Last Name">
            {getFieldDecorator('last', {
              rules: [
                {
                  required: true,
                  message: 'Please input the title of collection!',
                },
              ],
              initialValue: last,
            })(<Input />)}
          </FormItem>
          <FormItem label="Gender">
            {getFieldDecorator('gender', {
              required: true,
              initialValue: gender,
            })(
              <Radio.Group>
                <Radio value="male">Male</Radio>
                <Radio value="female">Female</Radio>
              </Radio.Group>
            )}
          </FormItem>
          <FormItem label="Email">
            {getFieldDecorator('email', {
              initialValue: email,

              rules: [
                {
                  type: 'email',
                  message: 'The input is not valid E-mail!',
                },
                {
                  required: true,
                  message: 'Please input your E-mail!',
                },
              ],
            })(<Input />)}
          </FormItem>
          <FormItem label="Date of birth">
            {getFieldDecorator('dob', {
              initialValue: moment(dob, dateFormat),
            })(
              <DatePicker
                format={dateFormat}
                dateRender={current => {
                  const style = {};
                  if (current.date() === 1) {
                    style.border = '1px solid #1890ff';
                    style.borderRadius = '50%';
                  }
                  return (
                    <div className="ant-calendar-date" style={style}>
                      {current.date()}
                    </div>
                  );
                }}
              />
            )}
          </FormItem>
          <FormItem label="Mobile">
            {getFieldDecorator('cell', {
              initialValue: mobile,
            })(<Input />)}
          </FormItem>
          <FormItem label="Avatar">
            {getFieldDecorator('picture', {
              initialValue: avatar,
            })(<Input type="textarea" />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator('id', {
              initialValue: id,
            })(<Input type="hidden" />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}
