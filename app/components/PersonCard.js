import React from 'react';
import { Card } from 'antd';
import { Button } from 'antd';
import './PersonCard.scss';

export default class PersonCard extends React.Component {
  render() {
    const personData = this.props.personData;
    const cardClass = this.props.cardClass;
    const actionButtonText = this.props.actionButtonText;

    const avatarStyle = {
      width: 165,
      height: 165,
    };
    return (
      <div className="card-container">
        <Card.Grid hoverable="true" type="inner" className={cardClass}>
          <div className="name-box">
            <p>
              <img
                alt="main_image"
                style={avatarStyle}
                src={personData.picture}
              />

              <span className="name">
                {personData.name.first} {personData.name.last}
              </span>
            </p>
          </div>
          <div className="btn-overlay">
            <div className="btn-container">
              <Button
                className="edit-btn"
                type="edit"
                onClick={() => this.props.handleEditOnClick(personData)}
              >
                Edit
              </Button>

              <Button
                className="delete-btn"
                type="edit"
                onClick={() => this.props.handleDeleteOnClick(personData)}
              >
                Delete
              </Button>
            </div>
          </div>
        </Card.Grid>
      </div>
    );
  }
}
