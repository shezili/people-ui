import React, { Component } from 'react';
import {
  Icon,
  Form,
  Radio,
  Modal,
  Input,
  Card,
  Button,
  message,
  Alert,
} from 'antd';
import PersonCard from './PersonCard';
import EditForm from './EditForm';
const Search = Input.Search;
const confirm = Modal.confirm;

const Config = require('Config');
const baseUri = Config.serverUrl;

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      results: [],
      personToEdit: null,
      loading: true,
      error: null,
      visible: false,
      confirmLoading: false,
    };
    this.delete = this.delete.bind(this);
  }

  componentDidMount() {
    const uri = `${baseUri}/people`;
    try {
      fetch(uri)
        .then(res => {
          if (!res.ok) {
            throw new Error('Failed to get data.');
          }
          return res.json();
        })
        .then(res => {
          this.setState({ results: res.results });
          console.log(this.setState.results);
          console.log(res.results);
          this.setState({ loading: false });
          this.setState({ error: null });
        })
        .catch(error => {
          this.setState({ error, loading: false });
        });
    } catch (error) {
      this.setState({ error, loading: false });
    }
  }
  delete(person) {
    const options = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      let uri = `${baseUri}/people/${person.id}`;
      fetch(uri, options).then(res => {
        if (!res.ok) {
          res.text().then(data => {
            message.error(`Error when delete data:${data}`, 10);
          });
        } else {
          const results = this.state.results;
          let indexToDelete = results.findIndex(ele => ele.id === person.id);

          if (indexToDelete >= 0) {
            results.splice(indexToDelete, 1);
          }
          this.setState({ results: results });

          message.success(
            `${person.name.first} ${person.name.last} is deleted.`
          );
        }
      });
    } catch (error) {
      error.then(err => {
        message.error(
          `Error when delete data:${JSON.stringify(err.text())}`,
          10
        );
      });
    }
  }
  deletePeople = person => {
    confirm({
      title: 'Are you sure delete this person?',
      content: 'This operation cannot rollback. Please be careful.',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () => {
        this.delete(person);
      },
      onCancel() {},
    });
  };

  showEdit = person => {
    this.setState({
      visible: true,
    });
    this.setState({ personToEdit: person });
  };

  showAdd = () => {
    this.setState({
      visible: true,
    });
    this.setState({ personToEdit: null });
  };

  handleOk = () => {
    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  handleCreate = () => {
    const form = this.formRef.props.form;
    let people = null;
    let isUpdate = false;

    form.validateFields((err, values) => {
      if (err) {
        return;
      }
    });
    const values = form.getFieldsValue();

    people = {
      gender: values.gender,
      name: {
        title: values.title,
        first: values.first,
        last: values.last,
      },
      email: values.email,
      dob: values.dob,
      cell: values.cell,
      id: values.id,
      picture: values.picture,
    };

    if (values.id) {
      isUpdate = true;
    }

    let uri = `${baseUri}/people`;

    if (isUpdate) {
      uri = `${uri}/${values.id}`;
    }

    const options = {
      body: JSON.stringify(people),
      method: isUpdate ? 'put' : 'post',
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      fetch(uri, options).then(res => {
        if (!res.ok) {
          res.text().then(data => {
            message.error(`Error when save data:${data}`, 10);
          });
        } else {
          form.resetFields();
          const results = this.state.results;

          if (!isUpdate) {
            results.push(values);
            message.success(`${values.first} ${values.last} is added.`);
          } else {
            let updatePerson = results.find(ele => ele.id === values.id);
            console.log(updatePerson);
            updatePerson.name.title = values.title;
            updatePerson.name.first = values.first;
            updatePerson.name.last = values.last;
            updatePerson.gender = values.gender;
            updatePerson.email = values.email;
            updatePerson.dob = values.dob;
            updatePerson.cell = values.cell;
            updatePerson.picture = values.picture;
            // for (var i in results) {
            //   if (results[i].id === values.id) {
            //     results[i].name.title = values.title;
            //     results[i].name.first = values.first;
            //     results[i].name.last = values.last;
            //     results[i].gender = values.gender;
            //     results[i].email = values.email;
            //     results[i].dob = values.dob;
            //     results[i].cell = values.cell;
            //     results[i].picture = values.picture;

            //     break;
            //   }
            // }
            message.success(`${values.first} ${values.last} is updated.`);
          }
          this.setState({ results: results });
          this.setState({ visible: false });
        }
      });
    } catch (error) {
      error.then(err => {
        message.error(`Error when save data:${JSON.stringify(err.text())}`, 10);
      });
    }
  };
  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    const { results, error, loading, visible, confirmLoading } = this.state;

    if (error) {
      return (
        <div>
          <Alert message="Oh no!" description={error.message} type="error" />
        </div>
      );
    }
    const PersonDetailsForm = Form.create()(EditForm);

    if (results) {
      return (
        <div className="app_container">
          <div className="main-container">
            <div className="list-container">
              <Card loading={this.state.loading} title="People List">
                <Button
                  type="primary"
                  icon="plus-circle-o"
                  onClick={this.showAdd}
                >
                  Add new person
                </Button>
                {results.map((person, index) => {
                  return (
                    <div key={index}>
                      <PersonCard
                        personData={person}
                        cardClass="person-card"
                        actionButtonText="Edit"
                        handleEditOnClick={this.showEdit}
                        handleDeleteOnClick={this.deletePeople}
                      />
                    </div>
                  );
                })}
              </Card>
              <PersonDetailsForm
                wrappedComponentRef={this.saveFormRef}
                visible={this.state.visible}
                onCancel={this.handleCancel}
                onCreate={this.handleCreate}
                personData={this.state.personToEdit}
              />
            </div>
          </div>
        </div>
      );
    }
    if (loading) {
      return <div>loading</div>;
    }
  }
}
